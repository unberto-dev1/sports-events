class StoiximanFeedClass extends elementorModules.frontend.handlers.Base {
    getDefaultSettings() {
        return {
            selectors: {
                container: ".sf-container"
            }
        };
    }

    getDefaultElements() {
        const selectors = this.getSettings("selectors");

        return {
            $container: this.$element.find(selectors.container)
        };
    }

    bindEvents() {
    	const $container = this.elements.$container[0];
    }
}

jQuery(window).on(
    "elementor/frontend/init",
    () => {
        const addHandler = $element => {
            elementorFrontend.elementsHandler.addHandler(
                StoiximanFeedClass,
                {
                    $element
                }
            );
        };

        elementorFrontend.hooks.addAction("frontend/element_ready/stoiximan-feed.default", addHandler);
    }
);
