document.addEventListener("DOMContentLoaded", function(event) { 
    (function($) {
        var live = false; // Live only
        var type = 'all';

        $('#sf--live-toggle').click(function(e){ 
            live = !live;
            sf_parse();
        });

        $('.sf-sport-selector').click(function(){
            $('.sf-sport-selectors').removeClass('open');
            $('.sf-sport-selectors a').removeClass('active');
            $(this).addClass('active');
            type = $(this).attr('href').replace('#','');
            sf_parse();
            return false;
        });

        $('.sf-sport-selector-all').click(function(){
            $('.sf-sport-selectors').removeClass('open');
            $('.sf-sport-selectors a').removeClass('active');
            $(this).addClass('active');
            type = 'all';
            sf_parse();
            return false;
        });

        

        function sf_parse(){
            $('.sf--entry').show();
            if( type != 'all' ){
                $('.sf--entry').not('.'+type).hide();
            }
            
            if(live){
                $('.sf--entry').not('.sf--live-item').hide();
            }
        }

        $('.sf-sport-selectors-toggle').click(function(){
            $('.sf-sport-selectors').toggleClass('open');
        });


        if( $( window ).width() > 768 ){
            $('.slick-wrapper').slick({
                variableWidth: true,
                infinite: true,
                //slidesToShow: 8,
                slidesToScroll: 1,
                swipe: false,
                prevArrow: $('.slick-prev'),
                nextArrow: $('.slick-next')
            });
        }
        
        

    })( jQuery );
});