<?php
/**
 * Plugin Name:     Stoiximan Feed
 * Plugin URI:      https://webfreebets.com/
 * Description:     Display the Stoixima Feed in Elementor.
 * Author:          KodeFor.Me
 * Author URI:      https://kodefor.me
 * Text Domain:     sf
 * Domain Path:     /languages
 * Version:         1.0.0
 *
 * @package         Stoiximan_Feed
 */

use SF\App;

define('SF_PLUGIN_DIR', plugins_url().'/stoiximan-feed/');

require_once "vendor/autoload.php";

$app = App::getInstance();

//if ( null !== $app->get('hooks') ) {
	$app->get('hooks')->init();

	add_action(
		'init',
		function () use ( $app ) {
			$app->get( 'elementor' )->init();
		}
	);
//}
