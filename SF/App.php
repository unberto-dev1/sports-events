<?php

namespace SF;

use PetrKnap\Php\Singleton\SingletonInterface;
use PetrKnap\Php\Singleton\SingletonTrait;
use Pimple\Container;
use SF\Elementor\Elementor;
use SF\General\Hooks;

class App implements SingletonInterface {
    use SingletonTrait;

    /**
     * @var Container $container
     */
    private $container;

    private function __construct() {
        $this->container = new Container();

        $this->setupContainer();

	    date_default_timezone_set('Europe/Athens');
    }

    /**
     * Responsible to load the container with services.
     */
    protected function setupContainer() {
        if ( ! did_action( 'elementor/loaded' ) ) {
            add_action( 'admin_notices', [ $this, 'admin_notice_missing_main_plugin' ] );
            return;
        }

        $this->container['elementor'] = function($c) {
            return new Elementor();
        };

        $this->container['hooks'] = function ($c) {
        	return new Hooks();
        };
    }

    /**
     * Responsible to check if the container has a given key.
     *
     * @param null $key The key to search for.
     *
     * @return bool Returns false if the key not exists in container or the key is null. Otherwise returns true.`
     */
    public function has( $key = null ) {
        if ( null === $key ) {
            return false;
        }

        return isset( $this->container[ $key ] );
    }

    /**
     * Responsible to return a value from the container using the given key.
     *
     * @param null $key The key to search for.
     *
     * @return mixed|null Returns null if the given key is null or doesn't exists in the container. Returns the found
     * object if the key exists.
     */
    public function get( $key = null ) {
        if ( null === $key ) {
            return null;
        }

        return $this->has($key) ? $this->container[$key] : null;
    }

    /**
     * Responsible to display a message to the dashboard users.
     */
    public function admin_notice_missing_main_plugin() {

        if ( isset( $_GET['activate'] ) ) unset( $_GET['activate'] );

        $message = sprintf(
        /* translators: 1: Plugin name 2: Elementor */
            esc_html__( '"%1$s" requires "%2$s" to be installed and activated.', 'sf' ),
            '<strong>' . esc_html__( 'Stoiximan Feed', 'sf' ) . '</strong>',
            '<strong>' . esc_html__( 'Elementor', 'sf' ) . '</strong>'
        );

        printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message );

    }
}
