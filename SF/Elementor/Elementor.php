<?php

namespace SF\Elementor;

use Elementor\Plugin;
use PetrKnap\Php\Singleton\SingletonInterface;
use PetrKnap\Php\Singleton\SingletonTrait;
use SF\Elementor\Widgets\StoiximanFeed;

class Elementor {
    public function init() {
        add_action('elementor/widgets/widgets_registered', [$this, $this->init_widgets()] );
    }

    public function init_widgets() {
        $files = glob(plugin_dir_path(__FILE__) . 'Widgets/*.php');

        foreach($files as $file) {
            $info  = pathinfo($file);
            $class = 'SF\\Elementor\\Widgets\\' . $info['filename'];
            Plugin::instance()->widgets_manager->register_widget_type(new $class());
        }
    }

    public function init_controls() {
        //Plugin::instance()->controls_manager->register_control();
    }
}
