<?php

namespace SF\Elementor\Widgets;

use DateTime;
use Elementor\Controls_Manager;
use Elementor\Core\Schemes\Color;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Typography;
use Elementor\Widget_Base;
use WP_Http;

class StoiximanFeed extends Widget_Base {

	public function __construct( $data = [], $args = null ) {
		parent::__construct( $data, $args );

		wp_register_script(
			'stoiximan-feed',
			plugins_url( 'stoiximan-feed' ) . '/assets/js/Elementor/dist/elementor.js',
			[
				'elementor-frontend',
			],
			'1.0.0',
			true
		);
		
		wp_register_script(
			'stoiximan-slick',
			plugins_url( 'stoiximan-feed' ) . '/assets/js/Elementor/dist/slick.min.js',
			[],
			'1.0.0',
			true
		);

		wp_register_script(
			'stoiximan-live',
			plugins_url( 'stoiximan-feed' ) . '/assets/js/Elementor/dist/live.js',
			[],
			'1.0.0',
			true
		);

		wp_register_style(
			'stoiximan-slick',
			plugins_url( 'stoiximan-feed' ) . '/assets/css/slick.css',
			[],
			'1.0.0'
		);

		wp_register_style(
			'stoiximan-feed',
			plugins_url( 'stoiximan-feed' ) . '/assets/css/style.min.css',
			[],
			'1.0.0'
		);

	
	}

	public function get_script_depends() {
		return [
			'stoiximan-feed',
			'stoiximan-slick',
			'stoiximan-live'
		];
	}

	public function get_style_depends() {
		return [
			'stoiximan-slick',
			'stoiximan-feed'
		];
	}

	/**
	 * @inheritDoc
	 */
	public function get_name() {
		return 'stoiximan-feed';
	}

	public function get_title() {
		return 'Stoiximan Feed';
	}

	public function get_icon() {
		return 'fa fa-code';
	}

	public function get_categories() {
		return [ 'general' ];
	}

	protected function _register_controls() {
		$this->generalSettings();
		$this->styleSettings();
	}

	protected function render() {
		$settings = $this->get_settings_for_display();
		$sport    = $settings['feed_sport'];
		$language = $settings['feed_language'];
		$max      = (int) $settings['feed_max_results'];
		$feedUrl_inp  = $settings['feed_url']['url'];
		$league_display = $settings['league_display'];
		//$feedUrl_inp = 'https://www.stoiximan.gr/adserve?type=LiveStreamingFeed';
		
		$sport_str = ($sport != 'ALL') ? '&sport='.$sport : '';
		$language_str = '&lang='.$language;
		$max_str = ( $max < 500 ) ? '&max='.$max : '&max=500';
		
		$feedUrl = $feedUrl_inp . $sport_str . $language_str . $max_str;

		$sports_list = [
			'FOOT' => ['icon' => '⚽', 'name' => 'Ποδόσφαιρο'], // Football
			'BASK' => ['icon' => '🏀', 'name' => 'Μπάσκετ'], // Basketball
			'TENN' => ['icon' => '🎾', 'name' => 'Τένις'], // Tennis
			'VOLL' => ['icon' => '<img src="'.SF_PLUGIN_DIR.'assets/images/volleyball.svg">', 'name' => 'Βόλεϊ'], // Volleyball
			'ESPS' => ['icon' => '<img src="'.SF_PLUGIN_DIR.'assets/images/game-controller.svg">', 'name' => 'Esports'],
			'HAND' => ['icon' => '<img src="'.SF_PLUGIN_DIR.'assets/images/soccer-ball.svg">', 'name' => 'Χάντμπολ'], // Handball
			'ICEH' => ['icon' => '🏒', 'name' => 'Χόκεϊ'], // Ice hockey
			'BASE' => ['icon' => '⚾', 'name' => 'Μπέιζμπολ'], // Baseball
			'BCHV' => ['icon' => '🏐', 'name' => 'Μπιτς Βόλεϊ'], // Beach volleyball
			'DART' => ['icon' => '🎯', 'name' => 'Darts'],
			'TABL' => ['icon' => '🏓', 'name' => 'Πινγκ Πονγκ'], // Table tennis
			'BADM' => ['icon' => '🏸', 'name' => 'Μπάντμιντον'], // Badminton
		];

		$entries  = $this->getFeedEntries( $feedUrl, $sport, $language );

		$parsedFeedURL = parse_url( $feedUrl, PHP_URL_QUERY ); ?>
		
		
		<div class="sf--top-bar">
			<div class="sf--live-toggle-container">
				<span class="top-bar-item-name"><?php _e( 'Μόνο LIVE', 'sf' )?></span>
				<label for="sf--live-toggle" class="sf--live-toggle-checkbox">
					<input id="sf--live-toggle" type="checkbox" autocomplete="off">
					<span class="sf--live-toggle-switch"></span>
				</label>
			</div>
			<?php if($sport == 'ALL'){ ?>
				<div class="sf-sport-selectors">
					<button type="button" class="slick-prev slick-arrow"></button>
					<div class="slick-wrapper">
						<div class="sf-sport-selector-wrp"><a href="#type-all" class="sf-sport-selector-all active"><?php echo _e('ΟΛΑ τα Αθλήματα', 'sf'); ?></a></div>
						<?php foreach($sports_list as $sport_k => $sport_v ){ ?>
							<div class="sf-sport-selector-wrp"><a href="#type-<?php echo strtolower($sport_k); ?>" class="sf-sport-selector"><?php echo $sport_v['icon']; ?> <?php _e($sport_v['name'], 'sf'); ?></a></div>
						<?php } ?>
					</div>
					<button type="button" class="slick-next slick-arrow"></button>
				</div>
				<button class="sf-sport-selectors-toggle"><span></span><span></span><span></span></button>
			<?php } ?>
		</div>
		<div class="sf sf--layout-<?php echo esc_attr( $settings['feed_layout'] ); ?>" data-query="<?php echo esc_attr( $parsedFeedURL ); ?>">

			<?php
			if ( empty( $entries ) ) {
				?>
				<div class="sf--no-games">
					<?php _e( 'No Games To Display', 'sf' ); ?>
				</div>
				<?php
			} else {
				$c = 0;
				foreach ( $entries as $entry ) {
					$c ++;
					$classes = [ 'sf--entry' ];
					$date = new DateTime();
					$date->setTimestamp( strtotime( $entry->date ) );
					$diff = new DateTime();
					$diff->setTimezone( $date->getTimezone() );
					$diffObj = $date->diff( $diff );
					
					$time_diff = ( $date->getTimestamp() - $diff->getTimestamp() <= 0 ) ? true : false; 
				
					

					if ( 0 === $c % 2 ) {
						array_push( $classes, 'sf--entry__odd' );
					}
					
					if ( $time_diff ) {
						array_push( $classes, 'sf--live-item' );
					}
					array_push( $classes, 'type-'.strtolower($entry->sport) );
					?>
					<div class="<?php echo esc_attr( implode( ' ', $classes ) ); ?>">
						<div class="sf--entry-time">
							<?php echo $date->format( 'H:i' ); ?><span class="mobile-only">,</span><br> <?php echo $date->format( 'd/m' ); ?>
						</div>
						<?php
						if ( $time_diff ) {
							?>
							<div class="sf--entry-live desktop">
								<?php if(!empty($settings['live_image']['url']) and !strpos($settings['live_image']['url'], 'placeholder.png')){ ?>
									<img src="<?php echo $settings['live_image']['url']; ?>" alt="<?php _e( 'Live', 'sf' ); ?>">
								<?php }else{ ?>
									<span class="sf--entry-live-badge"><?php _e( 'Live', 'sf' ); ?></span>
								<?php } ?>
							</div>
						<?php } ?>
						<div class="sf--entry-sport">
							<?php
							
							if( array_key_exists($entry->sport, $sports_list)){
								echo '<span class="sf--entry-sport-icon">'.$sports_list[$entry->sport]['icon'].'</span>'; 
								echo '<span class="sf--entry-sport-name">'.$sports_list[$entry->sport]['name'].'</span>';
							}else{
								echo $entry->sport;
							}
							
							?>
						</div>
						<div class="sf--entry-information">
							<a
								href="<?php echo esc_url( $settings['affiliate_url']['url'] . $entry->url ); ?>"
								title="<?php echo esc_attr( $entry->name ) . ' Live Streaming* στο Stoiximan'; ?>"
								target="_blank"
								rel="noreferrer noopener"
								class="sf--entry-link"
							>
								<span><?php echo $entry->name; ?></span>

								<?php
								if ( $time_diff ) {
									?>
									<span class="sf--entry-live mobile">
										<?php if(!empty($settings['live_image']) and !strpos($settings['live_image']['url'], 'placeholder.png')){ ?>
											<img src="<?php echo $settings['live_image']['url']; ?>" alt="<?php _e( 'Live', 'sf' ); ?>">
										<?php }else{ ?>
											<?php _e( 'Live', 'sf' ); ?>
										<?php } ?>
									</span>
								<?php } ?>
								
							</a>
							<?php if($league_display == 'yes'): ?>
							
							<span class="sf--entry-league">
								<?php echo $entry->leaguename; ?>
							</span>
							<?php endif; ?>
						</div>
						<div class="sf--watch-now">
							<a
								href="<?php echo esc_url( $settings['affiliate_url']['url'] . $entry->url ); ?>"
								title="<?php echo $settings['button_text']; ?>"
								target="_blank"
								rel="noreferrer noopener"><?php echo $settings['button_text']; ?></a>
						</div>
					</div>
					<?php
				}
			}
			?>
		</div>
		<?php
	}

	protected function generalSettings() {
		$this->start_controls_section(
			'general_settings',
			[
				'label' => __( 'General Settings', 'sf' ),
				'tab'   => Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'affiliate_url',
			[
				'type'        => Controls_Manager::URL,
				'label'       => __( 'Affiliate URL', 'sf' ),
				'description' => __( 'Enter the provided affiliate URL', 'sf' ),
				'label_block' => true,
				'placeholder' => __( 'Affiliate URL ...', 'sf' ),
				'default'     => [
					'url'         => 'https://gml-grp.com/C.ashx?btag=a_245b_65c_&affid=61&siteid=245&adid=65&c=&asclurl=',
					'is_external' => false,
					'nofollow'    => false,
				],
			]
		);

		$this->add_control(
			'feed_url',
			[
				'type'        => Controls_Manager::URL,
				'label'       => __( 'Feed URL', 'sf' ),
				'description' => __( 'Enter the provided feed URL', 'sf' ),
				'label_block' => true,
				'placeholder' => __( 'Feed URL ...', 'sf' ),
				'default'     => [
					'url'         => 'https://www.stoiximan.gr/adserve?type=LiveStreamingFeed',
					'is_external' => false,
					'nofollow'    => false,
				],
			]
		);

		$this->add_control(
			'feed_sport',
			[
				'type'        => Controls_Manager::SELECT,
				'label'       => __( 'Sports', 'sf' ),
				'description' => __( 'Choose the sport you like to display', 'sf' ),
				'label_block' => true,
				'options'     => [
					'ALL' => __( 'All sports', 'sf' ),
					'FOOT' => __( 'Football', 'sf' ),
					'BASK' => __( 'Basket', 'sf' ),
					'TENN' => __( 'Tennis', 'sf' ),
				],
				'default'     => 'ALL',
			]
		);

		$this->add_control(
			'feed_language',
			[
				'type'        => Controls_Manager::SELECT,
				'label'       => __( 'Language', 'sf' ),
				'description' => __( 'Choose the results language', 'sf' ),
				'label_block' => true,
				'options'     => [
					'el' => __( 'Greek', 'sf' ),
					'en' => __( 'English', 'sf' ),
				],
				'default'     => 'el',
			]
		);

		$this->add_control(
			'feed_max_results',
			[
				'type'        => Controls_Manager::NUMBER,
				'label'       => __( 'Max Results', 'sf' ),
				'description' => __( 'Enter how many records to return.', 'sf' ),
				'label_block' => true,
				'min'         => '1',
				'step'        => '1',
				'placeholder' => __( 'Max Results', 'sf' ),
				'title'       => __( 'Max Results', 'sf' ),
				'default'     => 10,
			]
		);

		$this->add_control(
			'feed_layout',
			[
				'type'        => Controls_Manager::SELECT,
				'label'       => __( 'Layout', 'sf' ),
				'description' => __( 'Choose the feed list layout', 'sf' ),
				'label_block' => true,
				'options'     => [
					'wide'   => __( 'Wide', 'sf' ),
					'narrow' => __( 'Narrow', 'sf' ),
				],
				'default'     => 'wide'
			]
		);

		$this->add_control(
			'league_display',
			[
				'type'        => Controls_Manager::SWITCHER,
				'label' => __( 'Display leagues', 'sf' ),
				'label_on' => __( 'Show', 'sf' ),
				'label_off' => __( 'Hide', 'sf' ),
				'return_value' => 'yes',
				'default' => 'yes'
			]
		);

		$this->end_controls_section();
	}

	protected function styleSettings() {
		$this->general_styles();
		$this->time_styles();
		$this->live_styles();
		$this->sport_styles();
		$this->description_styles();
		$this->button_styles();
	}

	protected function general_styles() {
		$this->start_controls_section(
			'general_style',
			[
				'label' => __( 'General', 'sf' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'columns_padding',
			[
				'label'              => __( 'Columns Padding', 'sf' ),
				'type'               => Controls_Manager::DIMENSIONS,
				'allowed_dimensions' => 'all',
				'size_units'         => [
					'px',
					'%',
					'em',
				],
				'default'            => [
					'top'      => 10,
					'bottom'   => 10,
					'left'     => 10,
					'right'    => 10,
					'unit'     => 'px',
					'isLinked' => true,
				],
				'desktop_default'    => [
					'top'      => 10,
					'bottom'   => 10,
					'left'     => 10,
					'right'    => 10,
					'unit'     => 'px',
					'isLinked' => true,
				],
				'tablet_default'     => [
					'top'      => 10,
					'bottom'   => 10,
					'left'     => 10,
					'right'    => 10,
					'unit'     => 'px',
					'isLinked' => true,
				],
				'mobile_default'     => [
					'top'      => 10,
					'bottom'   => 10,
					'left'     => 10,
					'right'    => 10,
					'unit'     => 'px',
					'isLinked' => true,
				],
				'selectors'          => [
					'{{WRAPPER}} .sf--entry > div' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();
	}

	protected function time_styles() {
		$this->start_controls_section(
			'time_style',
			[
				'label' => __( 'Time', 'sf' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'time_typography',
				'label'    => __( 'Typography', 'sf' ),
				'selector' => '{{WRAPPERS}} .sf--entry-time',
			]
		);

		$this->add_control(
			'time_color',
			[
				'label'     => __( 'Time Color', 'sf' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Color::get_type(),
					'value' => Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .sf--entry-time' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'time_bg',
			[
				'label'     => __( 'Time Background Color', 'sf' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Color::get_type(),
					'value' => Color::COLOR_2,
				],
				'selectors' => [
					'{{WRAPPER}} .sf--entry-time' => 'background-color: {{VALUE}}',
				],
			]
		);

		$this->add_responsive_control(
			'time_padding',
			[
				'label'              => __( 'Padding', 'sf' ),
				'type'               => Controls_Manager::DIMENSIONS,
				'allowed_dimensions' => 'all',
				'size_units'         => [
					'px',
					'%',
					'em',
				],
				'default'            => [
					'top'      => 10,
					'bottom'   => 10,
					'left'     => 10,
					'right'    => 10,
					'unit'     => 'px',
					'isLinked' => true,
				],
				'desktop_default'    => [
					'top'      => 10,
					'bottom'   => 10,
					'left'     => 10,
					'right'    => 10,
					'unit'     => 'px',
					'isLinked' => true,
				],
				'tablet_default'     => [
					'top'      => 10,
					'bottom'   => 10,
					'left'     => 10,
					'right'    => 10,
					'unit'     => 'px',
					'isLinked' => true,
				],
				'mobile_default'     => [
					'top'      => 10,
					'bottom'   => 10,
					'left'     => 10,
					'right'    => 10,
					'unit'     => 'px',
					'isLinked' => true,
				],
				'selectors'          => [
					'{{WRAPPER}} .sf--entry-time' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
				],
				'conditions'  => [
					'terms' => [
						[
							'name'     => 'feed_layout',
							'operator' => '=',
							'value'    => 'narrow',
						],
					],
				],
			]
		);

		$this->end_controls_section();
	}

	protected function live_styles() {
		$this->start_controls_section(
			'live_style',
			[
				'label' => __( 'Live', 'sf' ),
				'tab'   => Controls_Manager::TAB_STYLE,
				'conditions'  => [
					'terms' => [
						[
							'name'     => 'feed_layout',
							'operator' => '=',
							'value'    => 'wide',
						],
					],
				],
			]
		);

		$this->add_control(
			'live_image',
			[
				'label' => __( 'Choose Image', 'sf' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);

		$this->add_control(
			'live_bg',
			[
				'label'     => __( 'Background Color', 'sf' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Color::get_type(),
					'value' => Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .sf--entry-live' => 'background-color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'live_badge_bg',
			[
				'label'     => __( 'Live Badge Background Color', 'sf' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Color::get_type(),
					'value' => Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .sf--entry-live-badge' => 'background-color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'live_badge_color',
			[
				'label'     => __( 'Live Badge Color', 'sf' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Color::get_type(),
					'value' => Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .sf--entry-live-badge' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_responsive_control(
			'live_badge_padding',
			[
				'label'              => __( 'Padding', 'sf' ),
				'type'               => Controls_Manager::DIMENSIONS,
				'allowed_dimensions' => 'all',
				'size_units'         => [
					'px',
					'%',
					'em',
				],
				'default'            => [
					'top'      => 5,
					'bottom'   => 5,
					'left'     => 15,
					'right'    => 15,
					'unit'     => 'px',
					'isLinked' => false,
				],
				'desktop_default'    => [
					'top'      => 5,
					'bottom'   => 5,
					'left'     => 15,
					'right'    => 15,
					'unit'     => 'px',
					'isLinked' => false,
				],
				'tablet_default'     => [
					'top'      => 5,
					'bottom'   => 5,
					'left'     => 15,
					'right'    => 15,
					'unit'     => 'px',
					'isLinked' => false,
				],
				'mobile_default'     => [
					'top'      => 5,
					'bottom'   => 5,
					'left'     => 15,
					'right'    => 15,
					'unit'     => 'px',
					'isLinked' => false,
				],
				'selectors'          => [
					'{{WRAPPER}} .sf--entry-live-badge' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'live_typography',
				'label'    => __( 'Typography', 'sf' ),
				'selector' => '{{WRAPPER}} .sf--entry-live-badge',
			]
		);

		$this->end_controls_section();
	}

	protected function sport_styles() {
		$this->start_controls_section(
			'sport_style',
			[
				'label' => __( 'Sport', 'sf' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'sport_bg',
			[
				'label'     => __( 'Background Color', 'sf' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Color::get_type(),
					'value' => Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .sf--entry-sport' => 'background-color: {{VALUE}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'sport_icon_typography',
				'label'    => __( 'Icon Typography', 'sf' ),
				'selector' => '{{WRAPPER}} .sf--entry-sport-icon',
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'sport_name_typography',
				'label'    => __( 'Name Typography', 'sf' ),
				'selector' => '{{WRAPPER}} .sf--entry-sport-name',
			]
		);

		$this->add_control(
			'sport_name_color',
			[
				'label'     => __( 'Sport Name Color', 'sf' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Color::get_type(),
					'value' => Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .sf--entry-sport' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_responsive_control(
			'sport_padding',
			[
				'label'              => __( 'Padding', 'sf' ),
				'type'               => Controls_Manager::DIMENSIONS,
				'allowed_dimensions' => 'all',
				'size_units'         => [
					'px',
					'%',
					'em',
				],
				'default'            => [
					'top'      => 10,
					'bottom'   => 10,
					'left'     => 10,
					'right'    => 10,
					'unit'     => 'px',
					'isLinked' => true,
				],
				'desktop_default'    => [
					'top'      => 10,
					'bottom'   => 10,
					'left'     => 10,
					'right'    => 10,
					'unit'     => 'px',
					'isLinked' => true,
				],
				'tablet_default'     => [
					'top'      => 10,
					'bottom'   => 10,
					'left'     => 10,
					'right'    => 10,
					'unit'     => 'px',
					'isLinked' => true,
				],
				'mobile_default'     => [
					'top'      => 10,
					'bottom'   => 10,
					'left'     => 10,
					'right'    => 10,
					'unit'     => 'px',
					'isLinked' => true,
				],
				'selectors'          => [
					'{{WRAPPER}} .sf--entry-sport' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
				],
				'conditions'  => [
					'terms' => [
						[
							'name'     => 'feed_layout',
							'operator' => '=',
							'value'    => 'narrow',
						],
					],
				],
			]
		);

		$this->end_controls_section();
	}

	protected function description_styles() {
		$this->start_controls_section(
			'description_style',
			[
				'label' => __( 'Description', 'sf' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'description_bg',
			[
				'label'     => __( 'Background Color', 'sf' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Color::get_type(),
					'value' => Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .sf--entry-information' => 'background-color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'link_color',
			[
				'label'     => __( 'Link Color', 'sf' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Color::get_type(),
					'value' => Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .sf--entry-link' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'link_hover_color',
			[
				'label'     => __( 'Link Hover Color', 'sf' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Color::get_type(),
					'value' => Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .sf--entry-link:hover' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'description_link_typography',
				'label'    => __( 'Link Typography', 'sf' ),
				'selector' => '{{WRAPPER}} .sf--entry-link',
			]
		);

		$this->add_control(
			'league_color',
			[
				'label'     => __( 'League Color', 'sf' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Color::get_type(),
					'value' => Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .sf--entry-league' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'description_league_typography',
				'label'    => __( 'League Typography', 'sf' ),
				'selector' => '{{WRAPPER}} .sf--entry-league',
			]
		);

		$this->end_controls_section();
	}

	protected function button_styles() {
		$this->start_controls_section(
			'button_style',
			[
				'label' => __( 'Button', 'sf' ),
				'tab'   => Controls_Manager::TAB_STYLE,
				'conditions'  => [
					'terms' => [
						[
							'name'     => 'feed_layout',
							'operator' => '=',
							'value'    => 'wide',
						],
					],
				],
			]
		);

		$this->add_control(
			'button_text',
			[
				'label' => __( 'Button text', 'sf' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => 'ΔΕΙΤΕ ΕΔΩ'
			]
		);


		$this->add_control(
			'button_bg',
			[
				'label'     => __( 'Background Color', 'sf' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Color::get_type(),
					'value' => Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .sf--watch-now' => 'background-color: {{VALUE}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name'     => 'button_border',
				'label'    => __( 'Border', 'sf' ),
				'selector' => '{{WRAPPER}} .sf--watch-now a',
			]
		);

		$this->add_control(
			'btn_border_color',
			[
				'label'     => __( 'Border Color', 'sf' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Color::get_type(),
					'value' => Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .sf--watch-now a:hover' => 'border-color: {{VALUE}}',
				],
			]
		);

		$this->add_responsive_control(
			'button_padding',
			[
				'label'              => __( 'Padding', 'sf' ),
				'type'               => Controls_Manager::DIMENSIONS,
				'allowed_dimensions' => 'all',
				'size_units'         => [
					'px',
					'%',
					'em',
				],
				'default'            => [
					'top'      => 5,
					'bottom'   => 5,
					'left'     => 15,
					'right'    => 15,
					'unit'     => 'px',
					'isLinked' => false,
				],
				'desktop_default'    => [
					'top'      => 5,
					'bottom'   => 5,
					'left'     => 15,
					'right'    => 15,
					'unit'     => 'px',
					'isLinked' => false,
				],
				'tablet_default'     => [
					'top'      => 5,
					'bottom'   => 5,
					'left'     => 15,
					'right'    => 15,
					'unit'     => 'px',
					'isLinked' => false,
				],
				'mobile_default'     => [
					'top'      => 5,
					'bottom'   => 5,
					'left'     => 15,
					'right'    => 15,
					'unit'     => 'px',
					'isLinked' => false,
				],
				'selectors'          => [
					'{{WRAPPER}} .sf--watch-now a' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'button_bg_color',
			[
				'label'     => __( 'Button Background Color', 'sf' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Color::get_type(),
					'value' => Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .sf--watch-now a' => 'background-color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'button_bg_hover_color',
			[
				'label'     => __( 'Button Background Hover Color', 'sf' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Color::get_type(),
					'value' => Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .sf--watch-now a:hover' => 'background-color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'button_color',
			[
				'label'     => __( 'Button Color', 'sf' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Color::get_type(),
					'value' => Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .sf--watch-now a' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'button_hover_color',
			[
				'label'     => __( 'Button Hover Color', 'sf' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Color::get_type(),
					'value' => Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .sf--watch-now a:hover' => 'color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_section();
	}

	protected function getFeedEntries( $feedUrl, $sport, $language ) {
		
		$transient_id = 'stoiximan_feed_' . $sport . '-' . $language . '-' . $this->get_id();
		
		//if ( false === ( $results = get_transient( $transient_id ) ) ) {

			$response = wp_remote_get( $feedUrl );

			if ( is_wp_error( $response ) ) {
				return [];
			}
			
			$body = json_decode( $response['body'] );
			
			
			if($sport != 'ALL'){
				$results = array_filter(
					$body,
					function ( $game ) use ( $sport ) {
						return strtoupper( $game->sport ) === strtoupper( $sport );
					}
				);
			}else{
				$results = $body;
			}

			$results = array_map(
				function ( $game ) {
					$new_game = new \stdClass();

					$new_game->name       = $game->name;
					$new_game->date       = $game->date;
					$new_game->url        = $game->Url;
					$new_game->leaguename = $game->leaguename;
					$new_game->regionname = $game->regionname;
					$new_game->sport      = $game->sport;

					return $new_game;
				},
				$results
			);

			if ( ! empty( $results ) ) {
				set_transient( $transient_id, $results, MINUTE_IN_SECONDS * 30 );
			}
		//}

		return $results;
	}
}
