<?php


namespace SF\General;


class Hooks {

	public function init() {
		add_action( 'plugin_loaded', [ $this, 'load_translation_files' ] );
	}

	public function load_translation_files() {
		load_plugin_textdomain(
			'sf',
			false,
			basename( dirname( dirname( dirname( __FILE__ ) ) ) ) . '/languages/'
		);
	}
}
