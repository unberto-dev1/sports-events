��    ;      �  O   �        5   	  `   ?  _   �  c         d     r     �     �     �     �     �     �     �     �     �          )  $   E     j     z  	   �     �  !   �      �     �     �                          /     5     E     N     U     b  
   t          �     �     �     �     �     �     �     �     		     	     	     (	     /	     >	     E	     J	  
   `	  
   k	  	   v	     �	  s  �	  q   �  l   k  c   �  g   <     �     �     �     �     �  %         &  (   3  9   \     �  ,   �  `   �  d   @  L   �     �       	   %     /  t   >  H   �  B   �     ?     H     U     j  !   w     �  )   �     �     �     �  !        $  .   B  '   q     �  +   �  /   �  '   �  %   "  
   H  @   S     �     �     �     �     �  
   �     �           #     7     L     \     1          0   (           !           '   +   ,   %                        .       9             2   $   
   #               *   7             /              5                     &                                3                       6      4   )          "   8   -   	   :   ;    "%1$s" requires "%2$s" to be installed and activated. <span class="sf--entry-sport-icon">⚽</span> <span class="sf--entry-sport-name">Football</span> <span class="sf--entry-sport-icon">🎾</span> <span class="sf--entry-sport-name">Tennis</span> <span class="sf--entry-sport-icon">🏀</span> <span class="sf--entry-sport-name">Basketball</span> Affiliate URL Affiliate URL ... Background Color Basket Border Border Color Button Button Background Color Button Background Hover Color Button Color Button Hover Color Choose the feed list layout Choose the results language Choose the sport you like to display Columns Padding Description Elementor English Enter how many records to return. Enter the provided affiliate URL Enter the provided feed URL Feed URL Feed URL ... Football General General Settings Greek Icon Typography Language Layout League Color League Typography Link Color Link Hover Color Link Typography Live Live Badge Background Color Live Badge Color Max Results Name Typography Narrow No Games To Display Padding Sport Sport Name Color Sports Stoiximan Feed Tennis Time Time Background Color Time Color Typography Watch Now Wide Project-Id-Version: sf
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language-Team: KodeFor.Me <info@kodefor.me>
Last-Translator: KodeFor.Me <info@kodefor.me>
Report-Msgid-Bugs-To: https://kodefor.me/
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: __;_e;_ex:1,2c;_n:1,2;_n_noop:1,2;_nx:1,2,4c;_nx_noop:1,2,3c;_x:1,2c;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c
X-Poedit-SourceCharset: UTF-8
Plural-Forms: nplurals=2; plural=(n != 1);
PO-Revision-Date: 
Language: el
X-Generator: Poedit 2.0.6
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 "%1$s" απαιτεί το "%2$s" να είναι εγκατεστημένο και ενεργοποιημένο. <span class="sf--entry-sport-icon">⚽</span> <span class="sf--entry-sport-name">Ποδόσφαιρο</span> <span class="sf--entry-sport-icon">🎾</span> <span class="sf--entry-sport-name">Τένις</span> <span class="sf--entry-sport-icon">🏀</span> <span class="sf--entry-sport-name">Μπάσκετ</span> Affiliate URL Affiliate URL ... Χρώμα Φόντου Μπάσκετ Περίγραμμα Χρώμα Περιγράμματος Κουμπί Χρώμα Φόντου Κουμπιού Χρώμα Φόντου Αιώρησης Κουμπιού Χρώμα Κουμπιού Χρώμα Αιώρησης Κουμπιού Επίλεξε την δομή που θέλεις να έχουν τα αποτελέσματα Επίλεξε την γλώσσα που θέλεις να είναι τα αποτελέσματα Επίλεξε το άθλημα που θέλεις να προβάλεις Περιθώριο Στηλών Περιγραφή Elementor Αγγλικά Εισήγαγε το σύνολο των αποτελεσμάτων που θέλεις να επιστραφούν Εισήγαγε το affiliate URL  που σου έχουν δώσει Εισήγαγε το feed URL που σου έχουν δώσει Feed URL Feed URL ... Ποδόσφαιρο Γενικά Γενικές Ρυθμίσεις Ελληνικά Τυπογραφία Εικονιδίου Γλώσσα Δομή Χρώμα Λίγκας Τυπογραφία Λίγκας Χρώμα Συνδέσμου Χρώμα Αιώρησης Συνδέσμου Τυπογραφία Συνδέσμου Live Χρώμα Φόντου Σήματος Live Χρώμα Κειμένου Σήματος Live Σύνολο Αποτελεσμάτων Τυπογραφία Ονόματος Στενή Δεν υπάρχουν παιχνίδια για προβολή Περιθώριο Άθλημα Χρώμα Ονόματος Αθλήματα Stoiximan Feed Τένις Ώρα Χρώμα Φόντου Ώρας Χρώμα ώρας Τυπογραφία Δες Τώρα Φαρδιά 